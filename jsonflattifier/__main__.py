import sys

from jsonflattifier.main import cli

if __name__ == "__main__":
    sys.exit(cli())
