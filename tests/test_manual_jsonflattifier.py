import json

import jsonflattifier
from jsonflattifier.helpers import __print_json


def main():
    s = open("data/inputs-repository/4-in.json", "r").read()
    json_result = jsonflattifier.flattify(s)
    # csv_result = jsonflattifier.csvfy(json_result)

    json_object = json.loads(json_result)
    __print_json(json_object)


if __name__ == "__main__":
    main()
